<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;

class PostControlador extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }


    public function index(){

        $posts = Post::all();
        return view('index', compact(['posts']));
    }

    public function create(){
        //
    }

    public function store(Request $request){

        $post = new Post();
        $post->email = $request->input('email');
        $post->mensagem = $request->input('mensagem');
        $path = $request ->file('arquivo')->store('imagens', 'public');
        $post->arquivo = $path;
        $post->save();
        return redirect('/index');
    }

    public function download($id){

        $post = Post::find($id);
        if(isset($post)){
            $path = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($post->arquivo);
            return response()->download($path);
        }
        return redirect('/index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id){

        $post = Post::find($id);
        if(isset($post)){
            $arquivo = $post->arquivo;
            Storage::disk('public')->delete($arquivo);
            $post->delete();
        }
        return redirect('/index');
    }
}
