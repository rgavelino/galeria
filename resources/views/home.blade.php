@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Você está logado!
                </div>
            </div>
        </div>
    </div>
    {{-- <div>
        <!doctype html>
        <html lang="pt-br">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta charset="utf-8">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            <title>Álbum</title>
            <style>
                body { padding: 20px; }
                .navbar { margin-bottom: 20px; }
                :root { --jumbotron-padding-y: 10px; }
                .jumbotron {
                  padding-top: var(--jumbotron-padding-y);
                  padding-bottom: var(--jumbotron-padding-y);
                  margin-bottom: 0;
                  background-color: #fff;
                }
                @media (min-width: 768px) {
                  .jumbotron {
                    padding-top: calc(var(--jumbotron-padding-y) * 2);
                    padding-bottom: calc(var(--jumbotron-padding-y) * 2);
                  }
                }
                .jumbotron p:last-child { margin-bottom: 0; }
                .jumbotron-heading { font-weight: 300; }
                .jumbotron .container { max-width: 40rem; }
                .btn-card { margin: 4px; }
                .btn { margin-right: 5px; }
                footer { padding-top: 3rem; padding-bottom: 3rem; }
                footer p { margin-bottom: .25rem; }

            </style>
        </head>
        <body>
            <footer class="text-muted">
              <div class="container">
                <p class="float btn btn-md btn-outline-secondary">
                <a href="{{asset ('/index')}}">Ir para galeria</a>
                </p>
                <p></p>
              </div>
            </footer>

            <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
        </body>
        </html>
    </div> --}}
</div>
@endsection


